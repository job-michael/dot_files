#Git autocomplete
source /usr/local/git/contrib/completion/git-completion.bash
source /usr/local/git/contrib/completion/git-prompt.sh

#export PS1='\h:\w\$ '
export PS1='>> \[\033[0;32m\]\u\[\033[00m\] in \[\033[0;32m\]$( pwd ) ($( OUT=$( ls -A | wc -l ); echo $OUT ) entries, $(( $( ls -A | wc -l ) - $( ls | wc -l ) )) hidden) \[\033[1;31m\]$(__git_ps1 " (%s)") \n\[\033[1;32m\]\$\[\033[;m\] '

umask 022

export CLICOLOR=1

if [ -f ~/.aliases ]; then
. ~/.aliases
fi
