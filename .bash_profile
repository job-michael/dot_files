# Load .bashrc file
source $HOME/.bashrc

# Autocorrect path names when using `cd`
shopt -s cdspell

#Git path
PATH=/usr/local/git/bin:$PATH
