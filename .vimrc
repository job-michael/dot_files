if has("syntax")		" Syntax color
    syntax on
endif

set noerrorbells                " do not make any noise!

set visualbell                  " use the visual bell

set ruler                       " display current mouse position

set history=1000                " increase history

set undolevels=1000             " use many levels of undo

set autoread                    " if current file is modified by another IDE, vim will refresh it

set number 											" Display line numbers

set tabstop=2										" Tab contains 2 spaces

set mouse=a											" Scroll into VIM files
map <ScrollWheelUp> <C-Y>
map <ScrollWheelDown> <C-E> 
